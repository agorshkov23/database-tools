﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using AGorshkov23.SqlToMarkdown.Repositories;
using ConsoleMenu;

namespace AGorshkov23.SqlToMarkdown
{
    internal class Program
    {
        static void Main()
        {
            var list = ConfigurationManager.ConnectionStrings.OfType<ConnectionStringSettings>().ToList();

            var menu = new TypedMenu<ConnectionStringSettings>(list, "Выберете подключение", css => css.Name);
            var selectedConnectionString = menu.Display();

            var program = new Program();
            Console.WriteLine("Выбрана строка подключения " + selectedConnectionString.Name);
            program.Start(selectedConnectionString);
        }

        private void Start(ConnectionStringSettings connectionStringSettings)
        {
            Repository.Init(connectionStringSettings.ConnectionString);
            var tables = Repository.Table.GetAll("public");

            var date = DateTime.Now.ToString("yyyy-MM-dd-HH.mm.ss");
            var fileName = $"output {date}.md";
            var streamWriter = File.CreateText(fileName);

            foreach (var table in tables.OrderBy(table => table.TableName))
            {
                streamWriter.WriteLine($"# Таблица _{table.TableName}_{Environment.NewLine}");

                var comment = Repository.Comment.GetCommentByTable(table);
                if (!string.IsNullOrWhiteSpace(comment))
                    streamWriter.WriteLine($"{comment}.{Environment.NewLine}");
                else
                    Console.WriteLine("Нет комментария к таблице " + table.TableName);

                var columns = Repository.Column.GetColumnsByTable(table);

                streamWriter.WriteLine("№ | Столбец | Тип | NULL? | Комментарий");
                streamWriter.WriteLine("- | ------- | --- | ----- | -----------");
                foreach (var column in columns.OrderBy(column => column.OrdinalPosition))
                {
                    var columnComment = Repository.Comment.GetCommentByColumn(column);

                    if (string.IsNullOrWhiteSpace(columnComment))
                        Console.WriteLine("Нет комментария к столбцу " + column.TableName + "." + column.ColumnName);

                    streamWriter.WriteLine($"{column.OrdinalPosition} | **{column.ColumnName}** | {column.UdtName.ToUpperInvariant()} | {(column.CanBeNull ? "NULL" : "NOT NULL")} | {columnComment}");
                }

                streamWriter.WriteLine();
                streamWriter.WriteLine();
            }

            streamWriter.Flush();

            Process.Start(fileName);
        }
    }
}
