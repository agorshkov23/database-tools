﻿using FluentNHibernate.Mapping;

namespace AGorshkov23.SqlToMarkdown.Models.PgCatalog.Maps
{
    class PgDescriptionMap : ClassMap<PgDescription>
    {
        public PgDescriptionMap()
        {
            Not.LazyLoad();
            Schema("pg_catalog");
            Table("pg_description");

            CompositeId()
                .KeyProperty(description => description.ObjOId)
                .KeyProperty(description => description.ObjSubId);

            Map(description => description.ClassOId, "classoid");
            Map(description => description.Description, "description");
        }
    }

    class PgClassMap : ClassMap<PgClass>
    {
        public PgClassMap()
        {
            Not.LazyLoad();
            Schema("pg_catalog");
            Table("pg_class");

            Id(@class => @class.OId, "oid");
            Map(@class => @class.RelName, "relname");
        }
    }
}
