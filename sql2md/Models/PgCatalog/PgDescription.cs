﻿namespace AGorshkov23.SqlToMarkdown.Models.PgCatalog
{
    public class PgDescription
    {
        public long ObjOId { get; set; }
        public long ClassOId { get; set; }
        public int ObjSubId { get; set; }
        public string Description { get; set; }

        protected bool Equals(PgDescription other)
        {
            return ObjOId == other.ObjOId && ObjSubId == other.ObjSubId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PgDescription) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (ObjOId.GetHashCode()*397) ^ ObjSubId;
            }
        }
    }

    public class PgClass
    {
        public long OId { get; set; }
        public string RelName { get; set; }
    }
}
