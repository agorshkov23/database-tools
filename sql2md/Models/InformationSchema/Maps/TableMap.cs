﻿using FluentNHibernate.Mapping;

namespace AGorshkov23.SqlToMarkdown.Models.InformationSchema.Maps
{
    internal class TableMap : ClassMap<Table>
    {
        public TableMap()
        {
            Not.LazyLoad();

            Schema("information_schema");
            Table("tables");

            CompositeId()
                .KeyProperty(column => column.TableCatalog, "table_catalog")
                .KeyProperty(column => column.TableSchema, "table_schema")
                .KeyProperty(column => column.TableName, "table_name");
        }
    }
}
