﻿using AGorshkov23.SqlToMarkdown.Models.UserType;
using FluentNHibernate.Mapping;

namespace AGorshkov23.SqlToMarkdown.Models.InformationSchema.Maps
{
    internal class ColumnMap : ClassMap<Column>
    {
        public ColumnMap()
        {
            Not.LazyLoad();

            Schema("information_schema");
            Table("columns");

            CompositeId()
                .KeyProperty(column => column.TableCatalog, "table_catalog")
                .KeyProperty(column => column.TableSchema, "table_schema")
                .KeyProperty(column => column.TableName, "table_name")
                .KeyProperty(column => column.ColumnName, "column_name");

            Map(column => column.OrdinalPosition, "ordinal_position");
            Map(column => column.CanBeNull, "is_nullable").CustomType(typeof (YesNoType));
            Map(column => column.UdtName, "udt_name");
        }
    }
}
