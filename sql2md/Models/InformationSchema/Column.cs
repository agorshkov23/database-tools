﻿namespace AGorshkov23.SqlToMarkdown.Models.InformationSchema
{
    public class Column
    {
        public string TableCatalog { get; set; }
        public string TableSchema { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public int OrdinalPosition { get; set; }
        public bool CanBeNull { get; set; }
        
        /// <summary>
        /// Название базового типа столбца.
        /// </summary>
        public string UdtName { get; set; }

        protected bool Equals(Column other)
        {
            return string.Equals(TableCatalog, other.TableCatalog) && string.Equals(TableSchema, other.TableSchema) && string.Equals(TableName, other.TableName) && string.Equals(ColumnName, other.ColumnName);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Column) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (TableCatalog != null ? TableCatalog.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (TableSchema != null ? TableSchema.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (TableName != null ? TableName.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (ColumnName != null ? ColumnName.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}
