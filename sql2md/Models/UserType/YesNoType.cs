﻿using System;
using System.Data;
using NHibernate;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;

namespace AGorshkov23.SqlToMarkdown.Models.UserType
{
    public class YesNoType : IUserType
    {
        public SqlType[] SqlTypes => new[] { NHibernateUtil.String.SqlType };
        public Type ReturnedType => typeof(YesNoType);
        public bool IsMutable => false;

        public bool Equals(object x, object y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (x == null || y == null)
            {
                return false;
            }

            return x.Equals(y);
        }

        public int GetHashCode(object x)
        {
            return x?.GetHashCode() ?? typeof(bool).GetHashCode() + 473;
        }

        public object DeepCopy(object value)
        {
            return value;
        }

        public object Replace(object original, object target, object owner)
        {
            return original;
        }

        public object Assemble(object cached, object owner)
        {
            return cached;
        }

        public object Disassemble(object value)
        {
            return value;
        }

        public void NullSafeSet(IDbCommand cmd, object value, int index)
        {
            if (value == null)
            {
                ((IDataParameter)cmd.Parameters[index]).Value = DBNull.Value;
            }
            else
            {
                var yes = (bool)value;
                ((IDataParameter)cmd.Parameters[index]).Value = yes ? Yes : No;
            }
        }

        public object NullSafeGet(IDataReader rs, string[] names, object owner)
        {
            var value = NHibernateUtil.String.NullSafeGet(rs, names[0]);

            if (value == null)
            {
                return null;
            }

            var stringValue = (string)value;

            var isYes = stringValue.Equals(Yes, StringComparison.InvariantCultureIgnoreCase);
            var isNo = stringValue.Equals(No, StringComparison.InvariantCultureIgnoreCase);

            if (!isYes && !isNo)
            {
                throw new Exception($"Некорректное значение {stringValue}");
            }

            return isYes;
        }

        private const string No = "NO";
        private const string Yes = "YES";
    }
}
