﻿using System;

namespace AGorshkov23.SqlToMarkdown.Repositories
{
    class RepositoryException : Exception
    {
        public RepositoryException(string message) : base(message)
        {
        }
    }
}
