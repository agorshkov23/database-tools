﻿using System.Collections.Generic;
using System.Linq;
using AGorshkov23.SqlToMarkdown.Models.InformationSchema;

namespace AGorshkov23.SqlToMarkdown.Repositories
{
    public class TableRepository : Repository<Table>
    {
        public List<Table> GetAll(string schema)
        {
            return Execute(session => session.QueryOver<Table>().Where(table => table.TableSchema == schema).List().ToList());
        }
    }
}
