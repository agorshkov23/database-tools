﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;

namespace AGorshkov23.SqlToMarkdown.Repositories
{
    public abstract class Repository
    {
        public static void Init(string connectionString)
        {
            ConnectionString = connectionString;

            if (ConnectionString == null)
                throw new RepositoryException("Репозиторий не инициализирован");

            var fluentConfiguration = Fluently.Configure()
                .Database(PostgreSQLConfiguration.Standard.ConnectionString(ConnectionString))
                .Mappings(configuration => configuration.FluentMappings.AddFromAssemblyOf<Repository>());

            SessionFactory = fluentConfiguration.BuildSessionFactory();

            Column = new ColumnRepository();
            Comment = new CommentRepository();
            Table = new TableRepository();
        }

        public static ColumnRepository Column { get; private set; }
        public static CommentRepository Comment { get; private set; }
        public static TableRepository Table { get; private set; }

        public static string ConnectionString { get; private set; }

        protected static ISessionFactory SessionFactory;

        protected static ISession GetSession()
        {
            return SessionFactory.OpenSession();
        }

    }

    public class Repository<T> : Repository where T : class
    {
        public List<T> GetAll()
        {
            return Execute(session =>
            {
                var s = session.QueryOver<T>().List().ToList();
                return s;
            });
        }

        protected TResult Execute<TResult>(Func<ISession, TResult> func)
        {
            using (var session = GetSession())
            {
                return func(session);
            }
        }
    }
}
