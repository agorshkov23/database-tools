﻿using System.Collections.Generic;
using System.Linq;
using Column = AGorshkov23.SqlToMarkdown.Models.InformationSchema.Column;
using Table = AGorshkov23.SqlToMarkdown.Models.InformationSchema.Table;

namespace AGorshkov23.SqlToMarkdown.Repositories
{
    public class ColumnRepository : Repository<Column>
    {
        public List<Column> GetColumnsByTable(Table table)
        {
            return
                Execute(session =>
                    session
                        .QueryOver<Column>()
                        .Where(column =>
                            column.TableCatalog == table.TableCatalog
                            && column.TableSchema == table.TableSchema
                            && column.TableName == table.TableName)
                            .List())
                    .ToList();
        }
    }
}