﻿using System.Collections.Generic;
using System.Linq;
using AGorshkov23.SqlToMarkdown.Models.InformationSchema;
using NHibernate.Util;

namespace AGorshkov23.SqlToMarkdown.Repositories
{
    public class CommentRepository : Repository<string>
    {
        public string GetCommentByColumn(Column column)
        {
            return Execute(session =>
            {
                var query = "SELECT description " +
                            "FROM pg_description " +
                            "JOIN pg_class ON pg_description.objoid = pg_class.oid " +
                            "JOIN pg_namespace ON pg_class.relnamespace = pg_namespace.oid " +
                            "WHERE relname = '" + column.TableName + "' AND nspname = '" + column.TableSchema + "' AND objsubid = " + column.OrdinalPosition;

                return session.CreateSQLQuery(query).List().FirstOrNull() as string;
            });
        }

        public string GetCommentByTable(Table table)
        {
//            Execute(session => session.CreateSQLQuery("SELECT description" +
//"FROM pg_description" +
//  "JOIN pg_class ON pg_description.objoid = pg_class.oid" + 
//"WHERE relname = " + table.TableName).SetResultTransformer())
//            ;

            return (string) Execute(session =>
            {
                var ss =  session.CreateSQLQuery("SELECT description " +
                                                 "FROM pg_description " +
                                                 "JOIN pg_class ON pg_description.objoid = pg_class.oid " +
                                                 "WHERE objsubid = 0 AND relname = '" + table.TableName + "'")
                    .List();
                return ss.OfType<string>().FirstOrNull();
            });

        }
    }
}
